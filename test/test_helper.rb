ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __dir__)
require 'rails/test_help'

class ActiveSupport
  class TestCase
    # rubocop:disable Metrics/LineLength
    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    # rubocop:enable Metrics/LineLength
    fixtures :all

    # Add more helper methods to be used by all tests here...
  end
end
