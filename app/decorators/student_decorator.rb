class StudentDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def errormessage(why)
    return unless object.errors[why].any?
    h.content_tag :span, style: 'color:red;', class: 'error' do
      h.safe_join(object.errors[why], h.tag(:br))
    end
  end
end
