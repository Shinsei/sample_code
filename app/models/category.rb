class Category < ApplicationRecord
  has_many :students, dependent: :destroy
end
