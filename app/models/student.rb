class Student < ApplicationRecord
  belongs_to :category
  validates :name, length: { minimum: 3 }, presence: true
  validates :memo, length: { minimum: 3 }, presence: true
  validates :category_id, presence: true
end
