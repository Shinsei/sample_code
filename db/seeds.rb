# rubocop:disable Metrics/LineLength
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# rubocop:enable Metrics/LineLength
primary = Category.create(status: 'primary')
junior = Category.create(status: 'junior high')
Category.create(status: 'high')
Category.create(status: 'college')
Student.create(name: 'aaa', memo: 'test memo', category: primary)
Student.create(name: 'bbb', memo: 'good student', category: junior)
